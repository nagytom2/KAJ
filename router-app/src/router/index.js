import Vue from 'vue'
import Router from 'vue-router'
import AboutMe from '@/components/AboutMe.vue'
import Education from '@/components/Education.vue'
import Other from '@/components/Other.vue'
import ContactMe from '@/components/ContactMe.vue'

Vue.use(Router)

export default new Router({
  routes: [
    {
      path: '/aboutme',
      name: 'AboutMe',
      component: AboutMe
    },
    {
      path: '/contactme',
      name: 'ContactMe',
      component: ContactMe
    },
    {
      path: '/education',
      name: 'Education',
      component: Education
    },
    {
      path: '/other',
      name: 'Other',
      component: Other
    }
  ]
})
